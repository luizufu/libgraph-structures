#include <cassert>
#include <cppcoro/generator.hpp>
#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>
#include <set>


auto gen_has(cppcoro::generator<uint32_t> gen, std::set<uint32_t>&& values)
    -> bool
{
    for(const auto& x: gen)
    {
        if(values.count(x) == 0)
        {
            return false;
        }
    }

    return true;
}

auto gen_has(cppcoro::generator<std::pair<uint32_t, const uint32_t&>> gen, std::set<std::pair<uint32_t, uint32_t>>&& values)
    -> bool
{
    for(const auto& x: gen)
    {
        if(values.count(x) == 0)
        {
            return false;
        }
    }

    return true;
}

template<typename Graph>
void test_edge_unlabeled()
{
    Graph g(3);

    {
        assert(g.n_vertices() == 3);
        assert(g.n_edges() == 0);
    }

    {
        g.insert_edge(0, 2);
        g.insert_edge(1, 2);
        g.insert_edge(1, 0);
        g.insert_edge(1, 1);
        g.insert_edge(2, 2);

        assert(g.n_edges() == 5);

        assert(g.out_degree(0) == 1);
        assert(g.out_degree(1) == 3);
        assert(g.out_degree(2) == 1);

        assert(g.in_degree(0) == 1);
        assert(g.in_degree(1) == 1);
        assert(g.in_degree(2) == 3);
    }

    {
        assert(gen_has(g.out_neighbors(0), {2}));
        assert(gen_has(g.out_neighbors(1), {2, 0, 1}));
        assert(gen_has(g.out_neighbors(2), {2}));

        assert(gen_has(g.in_neighbors(0), {1}));
        assert(gen_has(g.in_neighbors(1), {1}));
        assert(gen_has(g.in_neighbors(2), {0, 1, 2}));
    }

    {
        g.delete_edge(1, 0);
        assert(g.n_edges() == 4);

        g.delete_edge(1, 0);
        assert(g.n_edges() == 4);

        g.delete_out_neighbors(1);
        assert(g.n_edges() == 2);

        g.delete_out_neighbors(1);
        assert(g.n_edges() == 2);

        g.delete_in_neighbors(2);
        assert(g.n_edges() == 0);

        g.delete_in_neighbors(2);
        assert(g.n_edges() == 0);
    }
}


template <template <typename> class Graph>
void test_edge_labeled()
{
    Graph<uint32_t> g(3);

    {
        assert(g.n_vertices() == 3);
        assert(g.n_edges() == 0);
    }

    {
        g.insert_edge(0, 2, 1);
        g.insert_edge(1, 2, 2);
        g.insert_edge(1, 0, 3);
        g.insert_edge(1, 1, 4);
        g.insert_edge(2, 2, 5);

        assert(g.n_edges() == 5);

        assert(g.out_degree(0) == 1);
        assert(g.out_degree(1) == 3);
        assert(g.out_degree(2) == 1);

        assert(g.in_degree(0) == 1);
        assert(g.in_degree(1) == 1);
        assert(g.in_degree(2) == 3);

        assert(*g.label(0, 2) == 1);
        assert(*g.label(1, 2) == 2);
        assert(*g.label(1, 0) == 3);
        assert(*g.label(1, 1) == 4);
        assert(*g.label(2, 2) == 5);

    }

    {
        assert(gen_has(g.out_neighbors(0), {{2, 1}}));
        assert(gen_has(g.out_neighbors(1), {{2, 2}, {0, 3}, {1, 4}}));
        assert(gen_has(g.out_neighbors(2), {{2, 5}}));

        assert(gen_has(g.in_neighbors(0), {{1, 3}}));
        assert(gen_has(g.in_neighbors(1), {{1, 4}}));
        assert(gen_has(g.in_neighbors(2), {{0, 1}, {1, 2}, {2, 5}}));
    }

    {
        g.delete_edge(1, 0);
        assert(g.n_edges() == 4);

        g.delete_edge(1, 0);
        assert(g.n_edges() == 4);

        g.delete_out_neighbors(1);
        assert(g.n_edges() == 2);

        g.delete_out_neighbors(1);
        assert(g.n_edges() == 2);

        g.delete_in_neighbors(2);
        assert(g.n_edges() == 0);

        g.delete_in_neighbors(2);
        assert(g.n_edges() == 0);
    }
}

auto main() -> int
{
    using namespace graph_structures;

    test_edge_unlabeled<adj_list<>>();
    test_edge_unlabeled<adj_matrix<>>();
    test_edge_unlabeled<edge_grid<>>();

    test_edge_labeled<adj_list>();
    test_edge_labeled<adj_matrix>();
    test_edge_labeled<edge_grid>();
}
