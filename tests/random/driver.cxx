#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include <cassert>
#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>
#include <random>
#include <vector>

template<typename Graph>
void test_edge_unlabeled(size_t n);

template<template <typename> class Graph>
void test_edge_labeled(size_t n);

auto main() -> int
{
    using namespace graph_structures;

    test_edge_unlabeled<adj_list<>>(2048);
    test_edge_unlabeled<adj_matrix<>>(2048);
    test_edge_unlabeled<edge_grid<>>(2048);

    test_edge_labeled<adj_list>(2048);
    test_edge_labeled<adj_matrix>(2048);
    test_edge_labeled<edge_grid>(2048);
}

auto next_probability() -> float
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r(), r(), r(), r(), r(), r()};
    static std::mt19937 gen(seed);
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(gen);
}

using edge = std::pair<uint32_t, uint32_t>;
using edge_stream = cppcoro::generator<edge>;

auto equals(const std::set<uint32_t>& s1, const std::set<uint32_t>& s2) -> bool
{
    auto it1 = s1.begin();
    auto end1 = s1.end();
    auto it2 = s2.begin();
    auto end2 = s2.end();

    for(; it1 != end1 && it2 != end2; ++it1, ++it2)
    {
        if(*it1 != *it2)
        {
            return false;
        }
    }

    return it1 == end1 && it2 == end2;
}

auto make_gnp(uint32_t n, float p) -> edge_stream
{
    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = 0; v < n; ++v)
        {
            if(next_probability() < p)
            {
                co_yield {u, v};
            }
        }
    }
}

template<typename Graph>
void test_edge_unlabeled(size_t n)
{
    boost::adjacency_list<boost::setS, boost::vecS, boost::bidirectionalS> gb;
    std::vector<edge> not_in_graph;
    std::vector<edge> samples_in_graph;

    Graph g(n);

    {
        for(auto e: make_gnp(n, 0.3F))
        {
            if(next_probability() < 0.3F)
            {
                not_in_graph.push_back(e);
            }
            else
            {
                g.insert_edge(e.first, e.second);
                boost::add_edge(e.first, e.second, gb);

                if(next_probability() < 0.3F)
                {
                    samples_in_graph.push_back(e);
                }
            }
        }
    }

    {
        assert(boost::num_vertices(gb) == g.n_vertices());
        assert(boost::num_edges(gb) == g.n_edges());

        for(auto e: samples_in_graph)
        {
            assert(g.has_edge(e.first, e.second)
                   == boost::edge(e.first, e.second, gb).second);
        }

        for(auto e: not_in_graph)
        {
            assert(g.has_edge(e.first, e.second)
                   == boost::edge(e.first, e.second, gb).second);
        }
    }

    {
        std::vector<uint32_t> sample_vertices;

        for(uint32_t u = 0; u < n; ++u)
        {
            {
                assert(boost::out_degree(u, gb) == g.out_degree(u));

                std::set<uint32_t> g_neighbors;
                std::set<uint32_t> boost_neighbors;

                for(uint32_t v: g.out_neighbors(u))
                {
                    g_neighbors.insert(v);
                }

                for(auto [it2, end2] = boost::out_edges(u, gb); it2 != end2;
                    ++it2)
                {
                    boost_neighbors.insert(it2->m_target);
                }

                assert(equals(g_neighbors, boost_neighbors));
            }

            {
                assert(boost::in_degree(u, gb) == g.in_degree(u));

                std::set<uint32_t> g_neighbors;
                std::set<uint32_t> boost_neighbors;

                for(uint32_t v: g.in_neighbors(u))
                {
                    g_neighbors.insert(v);
                }

                for(auto [it2, end2] = boost::in_edges(u, gb); it2 != end2;
                    ++it2)
                {
                    boost_neighbors.insert(it2->m_source);
                }

                assert(equals(g_neighbors, boost_neighbors));
            }

            if(u % static_cast<uint32_t>(n * 0.1F) == 0)
            {
                sample_vertices.push_back(u);
            }
        }

        bool in_out = false;

        for(auto u: sample_vertices)
        {
            if(in_out)
            {
                g.delete_in_neighbors(u);
                boost::remove_in_edge_if(
                    u,
                    [](auto /*unused*/) {
                        return true;
                    },
                    gb);

                assert(g.in_degree(u) == 0);
            }
            else
            {
                g.delete_out_neighbors(u);
                boost::remove_out_edge_if(
                    u,
                    [](auto /*unused*/) {
                        return true;
                    },
                    gb);

                assert(g.out_degree(u) == 0);
            }

            in_out = !in_out;

            assert(boost::num_vertices(gb) == g.n_vertices());
            assert(boost::num_edges(gb) == g.n_edges());
        }

        for(auto e: samples_in_graph)
        {
            g.delete_edge(e.first, e.second);
            boost::remove_edge(e.first, e.second, gb);
            assert(boost::num_edges(gb) == g.n_edges());
        }

        for(auto e: samples_in_graph)
        {
            assert(g.has_edge(e.first, e.second)
                   == boost::edge(e.first, e.second, gb).second);
        }

        for(auto [it, end] = boost::edges(gb); it != end; ++it)
        {
            assert(g.has_edge(it->m_source, it->m_target));
        }
    }
}

template<template <typename> class Graph>
void test_edge_labeled(size_t n)
{
    using boost_graph = boost::adjacency_list<boost::setS, boost::vecS, boost::bidirectionalS, boost::no_property, boost::property<boost::edge_name_t, uint32_t>>;
    boost_graph gb;
    std::vector<edge> not_in_graph;
    std::vector<edge> samples_in_graph;

    Graph<uint32_t> g(n);

    {
        for(auto e: make_gnp(n, 0.3F))
        {
            uint32_t label = next_probability() * 100;

            if(next_probability() < 0.3F)
            {
                not_in_graph.push_back(e);
            }
            else
            {
                g.insert_edge(e.first, e.second, label);
                boost::add_edge(e.first, e.second, label, gb);

                if(next_probability() < 0.3F)
                {
                    samples_in_graph.push_back(e);
                }
            }
        }
    }

    {
        assert(boost::num_vertices(gb) == g.n_vertices());
        assert(boost::num_edges(gb) == g.n_edges());

        auto labelmap = boost::get(boost::edge_name, gb);

        for(auto e: samples_in_graph)
        {
            auto boost_edge = boost::edge(e.first, e.second, gb);
            assert(g.has_edge(e.first, e.second) == boost_edge.second);
            assert(*g.label(e.first, e.second) == labelmap[boost_edge.first]);
        }

        for(auto e: not_in_graph)
        {
            assert(g.has_edge(e.first, e.second)
                   == boost::edge(e.first, e.second, gb).second);
        }
    }

    {
        std::vector<uint32_t> sample_vertices;

        for(uint32_t u = 0; u < n; ++u)
        {
            {
                assert(boost::out_degree(u, gb) == g.out_degree(u));

                std::set<uint32_t> g_neighbors;
                std::set<uint32_t> boost_neighbors;

                for(const auto& [v, label]: g.out_neighbors(u))
                {
                    g_neighbors.insert(v);
                }

                for(auto [it2, end2] = boost::out_edges(u, gb); it2 != end2;
                    ++it2)
                {
                    boost_neighbors.insert(it2->m_target);
                }

                assert(equals(g_neighbors, boost_neighbors));
            }

            {
                assert(boost::in_degree(u, gb) == g.in_degree(u));

                std::set<uint32_t> g_neighbors;
                std::set<uint32_t> boost_neighbors;

                for(const auto& [v, label]: g.in_neighbors(u))
                {
                    g_neighbors.insert(v);
                }

                for(auto [it2, end2] = boost::in_edges(u, gb); it2 != end2;
                    ++it2)
                {
                    boost_neighbors.insert(it2->m_source);
                }

                assert(equals(g_neighbors, boost_neighbors));
            }

            if(u % static_cast<uint32_t>(n * 0.1F) == 0)
            {
                sample_vertices.push_back(u);
            }
        }

        bool in_out = false;

        for(auto u: sample_vertices)
        {
            if(in_out)
            {
                g.delete_in_neighbors(u);
                boost::remove_in_edge_if(
                    u,
                    [](auto /*unused*/) {
                        return true;
                    },
                    gb);

                assert(g.in_degree(u) == 0);
            }
            else
            {
                g.delete_out_neighbors(u);
                boost::remove_out_edge_if(
                    u,
                    [](auto /*unused*/) {
                        return true;
                    },
                    gb);

                assert(g.out_degree(u) == 0);
            }

            in_out = !in_out;

            assert(boost::num_vertices(gb) == g.n_vertices());
            assert(boost::num_edges(gb) == g.n_edges());
        }

        for(auto e: samples_in_graph)
        {
            g.delete_edge(e.first, e.second);
            boost::remove_edge(e.first, e.second, gb);
            assert(boost::num_edges(gb) == g.n_edges());
        }

        for(auto e: samples_in_graph)
        {
            assert(g.has_edge(e.first, e.second)
                   == boost::edge(e.first, e.second, gb).second);
        }

        for(auto [it, end] = boost::edges(gb); it != end; ++it)
        {
            assert(g.has_edge(it->m_source, it->m_target));
        }
    }
}
