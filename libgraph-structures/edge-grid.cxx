#include <libgraph-structures/edge-grid.hxx>

#include <cmath>
#include <utility>

namespace graph_structures
{

template<typename Container, typename Pred>
static auto erase_if(Container* s, Pred pred) -> uint32_t
{
    auto old_size = s->size();
    for(auto i = s->begin(), last = s->end(); i != last;)
    {
        if(pred(*i))
        {
            i = s->erase(i);
        }
        else
        {
            ++i;
        }
    }

    return old_size - s->size();
}

edge_grid<void>::edge_grid(uint32_t n, uint32_t block_size)
    : _grid(std::pow(std::ceil(n / static_cast<float>(block_size)), 2))
    , _block_size(block_size)
    , _n_grid(std::ceil(n / static_cast<float>(block_size)))
    , _n(n)
    , _m(0)
{
}

void edge_grid<void>::insert_edge(uint32_t u, uint32_t v)
{
    if(_grid[detail::idx(u / _block_size, v / _block_size, _n_grid)].insert({u, v}).second)
    {
        ++_m;
    }
}

void edge_grid<void>::delete_edge(uint32_t u, uint32_t v)
{
    if(_grid[detail::idx(u / _block_size, v / _block_size, _n_grid)].erase({u, v}) > 0)
    {
        --_m;
    }
}

void edge_grid<void>::delete_out_neighbors(uint32_t u)
{
    for(uint32_t v_id = 0; v_id < _n_grid; ++v_id)
    {
        _m -= erase_if(&_grid[detail::idx(u / _block_size, v_id, _n_grid)], [u](auto& e) {
            return e.first == u;
        });

    }
}

void edge_grid<void>::delete_in_neighbors(uint32_t v)
{
    for(uint32_t u_id = 0; u_id < _n_grid; ++u_id)
    {
        _m -= erase_if(&_grid[detail::idx(u_id, v / _block_size, _n_grid)], [v](auto& e) {
            return e.second == v;
        });
    }
}

auto edge_grid<void>::has_edge(uint32_t u, uint32_t v) const -> bool
{
    return _grid[detail::idx(u / _block_size, v / _block_size, _n_grid)].contains({u, v});
}

auto edge_grid<void>::out_neighbors(uint32_t u) const -> vertex_stream
{
    for(uint32_t v_id = 0; v_id < _n_grid; ++v_id)
    {
        for(auto e: _grid[detail::idx(u / _block_size, v_id, _n_grid)])
        {
            if(e.first == u)
            {
                co_yield e.second;
            }
        }
    }
}

auto edge_grid<void>::in_neighbors(uint32_t v) const -> vertex_stream
{
    for(uint32_t u_id = 0; u_id < _n_grid; ++u_id)
    {
        for(auto e: _grid[detail::idx(u_id, v / _block_size, _n_grid)])
        {
            if(e.second == v)
            {
                co_yield e.first;
            }
        }
    }
}

auto edge_grid<void>::edges() const -> edge_stream
{
    for(uint32_t u_id = 0; u_id < _n_grid; ++u_id)
    {
        for(uint32_t v_id = 0; v_id < _n_grid; ++v_id)
        {
            for(auto e: _grid[detail::idx(u_id, v_id, _n_grid)])
            {
                co_yield e;
            }
        }
    }
}

auto edge_grid<void>::out_degree(uint32_t u) const -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& v : out_neighbors(u))
    {
        ++degree;
    }

    return degree;
}

auto edge_grid<void>::in_degree(uint32_t v) const -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& u : in_neighbors(v))
    {
        ++degree;
    }

    return degree;
}

auto edge_grid<void>::n_vertices() const -> uint32_t
{
    return _n;
}

auto edge_grid<void>::n_edges() const -> uint32_t
{
    return _m;
}

} // namespace graph_structures
