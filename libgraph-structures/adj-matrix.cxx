#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/detail/utility.hxx>

namespace graph_structures
{
adj_matrix<void>::adj_matrix(uint32_t n)
    : _matrix(n * n, false)
    , _n(n)
    , _m(0)
{
}

void adj_matrix<void>::insert_edge(uint32_t u, uint32_t v)
{
    if(!_matrix[detail::idx(u, v, _n)])
    {
        _matrix[detail::idx(u, v, _n)] = true;
        ++_m;
    }
}

void adj_matrix<void>::delete_edge(uint32_t u, uint32_t v)
{
    if(_matrix[detail::idx(u, v, _n)])
    {
        _matrix[detail::idx(u, v, _n)] = false;
        --_m;
    }
}

void adj_matrix<void>::delete_out_neighbors(uint32_t u)
{
    for(uint32_t v = 0; v < _n; ++v)
    {
        delete_edge(u, v);
    }
}

void adj_matrix<void>::delete_in_neighbors(uint32_t v)
{
    for(uint32_t u = 0; u < _n; ++u)
    {
        delete_edge(u, v);
    }
}

auto adj_matrix<void>::has_edge(uint32_t u, uint32_t v) const -> bool
{
    return _matrix[detail::idx(u, v, _n)];
}

auto adj_matrix<void>::out_neighbors(uint32_t u) const -> vertex_stream
{
    for(uint32_t v = 0; v < _n; ++v)
    {
        if(_matrix[detail::idx(u, v, _n)])
        {
            co_yield v;
        }
    }
}

auto adj_matrix<void>::in_neighbors(uint32_t v) const -> vertex_stream
{
    for(uint32_t u = 0; u < _n; ++u)
    {
        if(_matrix[detail::idx(u, v, _n)])
        {
            co_yield u;
        }
    }
}

auto adj_matrix<void>::edges() const -> edge_stream
{
    for(uint32_t u = 0; u < _n; ++u)
    {
        for(uint32_t v = 0; v < _n; ++v)
        {
            if(_matrix[detail::idx(u, v, _n)])
            {
                co_yield {u, v};
            }
        }
    }
}

auto adj_matrix<void>::out_degree(uint32_t u) const -> uint32_t
{
    uint32_t degree = 0;
    for(uint32_t v = 0; v < _n; ++v)
    {
        if(_matrix[detail::idx(u, v, _n)])
        {
            ++degree;
        }
    }

    return degree;
}

auto adj_matrix<void>::in_degree(uint32_t v) const -> uint32_t
{
    uint32_t degree = 0;
    for(uint32_t u = 0; u < _n; ++u)
    {
        if(_matrix[detail::idx(u, v, _n)])
        {
            ++degree;
        }
    }

    return degree;
}

auto adj_matrix<void>::n_vertices() const -> uint32_t
{
    return _n;
}

auto adj_matrix<void>::n_edges() const -> uint32_t
{
    return _m;
}

} // namespace graph_structures
