#pragma once

#include <cppcoro/generator.hpp>
#include <vector>
#include <set>
#include <map>


namespace graph_structures
{

template <typename Label = void>
class edge_grid;

template <>
class edge_grid<void>
{
    using edge_set = std::set<std::pair<uint32_t, uint32_t>>;

public:
    using vertex_stream = cppcoro::generator<uint32_t>;
    using edge_stream = cppcoro::generator<std::pair<uint32_t, uint32_t>>;

    explicit edge_grid(uint32_t n, uint32_t block_size = 32U);

    void insert_edge(uint32_t u, uint32_t v);
    void delete_edge(uint32_t u, uint32_t v);

    void delete_out_neighbors(uint32_t u);
    void delete_in_neighbors(uint32_t v);

    [[nodiscard]] auto has_edge(uint32_t u, uint32_t v) const -> bool;

    [[nodiscard]] auto out_neighbors(uint32_t u) const -> vertex_stream;
    [[nodiscard]] auto in_neighbors(uint32_t v) const -> vertex_stream;
    [[nodiscard]] auto edges() const -> edge_stream;

    [[nodiscard]] auto out_degree(uint32_t u) const -> uint32_t;
    [[nodiscard]] auto in_degree(uint32_t v) const -> uint32_t;

    [[nodiscard]] auto n_vertices() const -> uint32_t;
    [[nodiscard]] auto n_edges() const -> uint32_t;

private:
    std::vector<edge_set> _grid;
    uint32_t _n_grid;
    uint32_t _block_size;
    uint32_t _n;
    uint32_t _m;
};


template <typename Label>
class edge_grid
{
    using edge_set = std::map<std::pair<uint32_t, uint32_t>, Label>;

public:
    using vertex_stream = cppcoro::generator<std::pair<uint32_t, const Label&>>;
    using edge_stream = cppcoro::generator<std::tuple<uint32_t, uint32_t, const Label&>>;

    explicit edge_grid(uint32_t n, uint32_t block_size = 32U);
    ~edge_grid();

    void insert_edge(uint32_t u, uint32_t v, const Label& label);
    void delete_edge(uint32_t u, uint32_t v);

    void delete_out_neighbors(uint32_t u);
    void delete_in_neighbors(uint32_t v);

    [[nodiscard]] auto has_edge(uint32_t u, uint32_t v) const -> bool;

    [[nodiscard]] auto label(uint32_t u, uint32_t v) -> Label*;
    [[nodiscard]] auto label(uint32_t u, uint32_t v) const -> const Label*;

    [[nodiscard]] auto out_neighbors(uint32_t u) const -> vertex_stream;
    [[nodiscard]] auto in_neighbors(uint32_t v) const -> vertex_stream;
    [[nodiscard]] auto edges() const -> edge_stream;

    [[nodiscard]] auto out_degree(uint32_t u) const -> uint32_t;
    [[nodiscard]] auto in_degree(uint32_t v) const -> uint32_t;

    [[nodiscard]] auto n_vertices() const -> uint32_t;
    [[nodiscard]] auto n_edges() const -> uint32_t;

#ifdef BENCHMARK_ON
    inline static uint64_t total_stack_usage = 0;
    inline static uint64_t total_heap_usage = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
#endif

private:
    std::vector<edge_set> _grid;
    uint32_t _n_grid;
    uint32_t _block_size;
    uint32_t _n;
    uint32_t _m;
};

} // namespace graph_structures

#include <libgraph-structures/edge-grid.ixx>
