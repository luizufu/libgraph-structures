#include <libgraph-structures/detail/utility.hxx>

namespace graph_structures
{

template <typename Label>
adj_matrix<Label>::adj_matrix(uint32_t n)
    : _matrix(n * n, std::nullopt)
    , _n(n)
    , _m(0)
{
#ifdef BENCHMARK_ON
    total_stack_usage += sizeof(_n) + sizeof(_m);
    total_heap_usage += _n * _n * sizeof(Label);
#endif
}

template <typename Label>
adj_matrix<Label>::~adj_matrix()
{
#ifdef BENCHMARK_ON
    total_stack_usage -= sizeof(_n) + sizeof(_m);
    total_heap_usage -= _n * _n * sizeof(Label);
#endif
}

template <typename Label>
void adj_matrix<Label>::insert_edge(uint32_t u, uint32_t v, const Label& label)
{
    if(!_matrix[detail::idx(u, v, _n)])
    {
        _matrix[detail::idx(u, v, _n)] = label;
        ++_m;
    }
}

template <typename Label>
void adj_matrix<Label>::delete_edge(uint32_t u, uint32_t v)
{
    if(_matrix[detail::idx(u, v, _n)])
    {
        _matrix[detail::idx(u, v, _n)] = std::nullopt;
        --_m;
    }
}

template <typename Label>
void adj_matrix<Label>::delete_out_neighbors(uint32_t u)
{
    for(uint32_t v = 0; v < _n; ++v)
    {
        delete_edge(u, v);
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif
}

template <typename Label>
void adj_matrix<Label>::delete_in_neighbors(uint32_t v)
{
    for(uint32_t u = 0; u < _n; ++u)
    {
        delete_edge(u, v);
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += _n;
#endif
}

template <typename Label>
auto adj_matrix<Label>::has_edge(uint32_t u, uint32_t v) const -> bool
{
    return _matrix[detail::idx(u, v, _n)].has_value();
}

template <typename Label>
auto adj_matrix<Label>::label(uint32_t u, uint32_t v) -> Label*
{
    auto& lab = _matrix[detail::idx(u, v, _n)];
    return lab.has_value() ? &(*lab) : nullptr;
}

template <typename Label>
auto adj_matrix<Label>::label(uint32_t u, uint32_t v) const -> const Label*
{
    const auto& lab = _matrix[detail::idx(u, v, _n)];
    return lab.has_value() ? &(*lab) : nullptr;
}

template <typename Label>
auto adj_matrix<Label>::out_neighbors(uint32_t u) const -> vertex_stream
{
    for(uint32_t v = 0; v < _n; ++v)
    {
        if(const auto& label = _matrix[detail::idx(u, v, _n)])
        {
            co_yield { v, *label };
        }
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif
}

template <typename Label>
auto adj_matrix<Label>::in_neighbors(uint32_t v) const -> vertex_stream
{
    for(uint32_t u = 0; u < _n; ++u)
    {
        if(const auto& label = _matrix[detail::idx(u, v, _n)])
        {
            co_yield { u, *label };
        }
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += _n;
#endif
}

template <typename Label>
auto adj_matrix<Label>::edges() const -> edge_stream
{
    for(uint32_t u = 0; u < _n; ++u)
    {
        for(uint32_t v = 0; v < _n; ++v)
        {
            if(const auto& label = _matrix[detail::idx(u, v, _n)])
            {
                co_yield { u, v, *label };
            }
        }
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += _n;
#endif
}

template <typename Label>
auto adj_matrix<Label>::out_degree(uint32_t u) const -> uint32_t
{
    uint32_t degree = 0;
    for(uint32_t v = 0; v < _n; ++v)
    {
        if(_matrix[detail::idx(u, v, _n)].has_value())
        {
            ++degree;
        }
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif

    return degree;
}

template <typename Label>
auto adj_matrix<Label>::in_degree(uint32_t v) const -> uint32_t
{
    uint32_t degree = 0;
    for(uint32_t u = 0; u < _n; ++u)
    {
        if(_matrix[detail::idx(u, v, _n)].has_value())
        {
            ++degree;
        }
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += _n;
#endif

    return degree;
}

template <typename Label>
auto adj_matrix<Label>::n_vertices() const -> uint32_t
{
    return _n;
}

template <typename Label>
auto adj_matrix<Label>::n_edges() const -> uint32_t
{
    return _m;
}

} // namespace graph_structures
