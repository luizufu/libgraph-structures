#pragma once

#include <bits/stdint-uintn.h>

namespace graph_structures::detail
{

inline auto idx(uint32_t i, uint32_t j, uint32_t n) -> uint32_t
{
    return i * n + j;
}

} // namespace graph_structures::detail
