#include <libgraph-structures/adj-list.hxx>

namespace graph_structures
{

adj_list<void>::adj_list(uint32_t n)
    : _lists(n)
    , _n_edges(0)
{
}

void adj_list<void>::insert_edge(uint32_t u, uint32_t v)
{
    if(_lists[u].insert(v).second)
    {
        ++_n_edges;
    }
}

void adj_list<void>::delete_edge(uint32_t u, uint32_t v)
{
    if(_lists[u].erase(v) > 0)
    {
        --_n_edges;
    }
}

void adj_list<void>::delete_out_neighbors(uint32_t u)
{
    _n_edges -= _lists[u].size();
    _lists[u].clear();
}

void adj_list<void>::delete_in_neighbors(uint32_t v)
{
    for(uint32_t u = 0; u < _lists.size(); ++u)
    {
        delete_edge(u, v);
    }
}

auto adj_list<void>::has_edge(uint32_t u, uint32_t v) const -> bool
{
    return _lists[u].contains(v);
}

auto adj_list<void>::out_neighbors(uint32_t u) const -> vertex_stream
{
    for(uint32_t v: _lists.at(u))
    {
        co_yield v;
    }
}

auto adj_list<void>::in_neighbors(uint32_t v) const -> vertex_stream
{
    for(uint32_t u = 0; u < _lists.size(); ++u)
    {
        if(has_edge(u, v))
        {
            co_yield u;
        }
    }
}

auto adj_list<void>::edges() const -> edge_stream
{
    for(uint32_t u = 0; u < _lists.size(); ++u)
    {
        for(uint32_t v: _lists[u])
        {
            co_yield {u, v};
        }
    }
}

auto adj_list<void>::out_degree(uint32_t u) const -> uint32_t
{
    return _lists[u].size();
}

auto adj_list<void>::in_degree(uint32_t v) const -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& adj: _lists)
    {
        if(adj.contains(v))
        {
            ++degree;
        }
    }

    return degree;
}

auto adj_list<void>::n_vertices() const -> uint32_t
{
    return _lists.size();
}

auto adj_list<void>::n_edges() const -> uint32_t
{
    return _n_edges;
}

} // namespace graph_structures
