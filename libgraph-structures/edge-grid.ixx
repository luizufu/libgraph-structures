#include <libgraph-structures/detail/utility.hxx>
#include <cmath>
#include <utility>

namespace graph_structures::detail
{

template<typename Container, typename Pred>
auto erase_if(Container* s, Pred pred) -> uint32_t
{
    auto old_size = s->size();
    for(auto i = s->begin(), last = s->end(); i != last;)
    {
        if(pred(i->first))
        {
            i = s->erase(i);
        }
        else
        {
            ++i;
        }
    }

    return old_size - s->size();
}

}  // namespace graph_structures::detail

namespace graph_structures
{

template <typename Label>
edge_grid<Label>::edge_grid(uint32_t n, uint32_t block_size)
    : _grid(std::pow(std::ceil(n / static_cast<float>(block_size)), 2))
    , _block_size(block_size)
    , _n_grid(std::ceil(n / static_cast<float>(block_size)))
    , _n(n)
    , _m(0)
{
#ifdef BENCHMARK_ON
    total_stack_usage += sizeof(_block_size) + sizeof(_n_grid) + sizeof(_n) + sizeof(_m);
#endif
}

template <typename Label>
edge_grid<Label>::~edge_grid()
{
#ifdef BENCHMARK_ON
    total_stack_usage -= sizeof(_block_size) + sizeof(_n_grid) + sizeof(_n) + sizeof(_m);
#endif
}

template <typename Label>
void edge_grid<Label>::insert_edge(uint32_t u, uint32_t v, const Label& label)
{
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    if(_grid[detail::idx(u / _block_size, v / _block_size, _n_grid)].insert({{u, v}, label}).second)
    {
        ++_m;
    }
}

template <typename Label>
void edge_grid<Label>::delete_edge(uint32_t u, uint32_t v)
{
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    if(_grid[detail::idx(u / _block_size, v / _block_size, _n_grid)].erase({u, v}) > 0)
    {
        --_m;
    }
}

template <typename Label>
void edge_grid<Label>::delete_out_neighbors(uint32_t u)
{
    for(uint32_t v_id = 0; v_id < _n_grid; ++v_id)
    {
        _m -= detail::erase_if(&_grid[detail::idx(u / _block_size, v_id, _n_grid)], [u](auto& e) {
            return e.first == u;
        });
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += std::min(_block_size, _n);
#endif
}

template <typename Label>
void edge_grid<Label>::delete_in_neighbors(uint32_t v)
{
    for(uint32_t u_id = 0; u_id < _n_grid; ++u_id)
    {
        _m -= detail::erase_if(&_grid[detail::idx(u_id, v / _block_size, _n_grid)], [v](auto& e) {
            return e.second == v;
        });
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += std::min(_block_size, _n);
#endif
}

template <typename Label>
auto edge_grid<Label>::has_edge(uint32_t u, uint32_t v) const -> bool
{
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    return _grid[detail::idx(u / _block_size, v / _block_size, _n_grid)].contains({u, v});
}

template <typename Label>
auto edge_grid<Label>::label(uint32_t u, uint32_t v) -> Label*
{
    auto& cell = _grid[detail::idx(u / _block_size, v / _block_size, _n_grid)];
    auto it = cell.find({u, v});
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    return it != cell.end() ? &it->second : nullptr;
}

template <typename Label>
auto edge_grid<Label>::label(uint32_t u, uint32_t v) const -> const Label*
{
    const auto& cell = _grid[detail::idx(u / _block_size, v / _block_size, _n_grid)];
    auto it = cell.find({u, v});
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    return it != cell.end() ? &it->second : nullptr;
}

template <typename Label>
auto edge_grid<Label>::out_neighbors(uint32_t u) const -> vertex_stream
{
    for(uint32_t v_id = 0; v_id < _n_grid; ++v_id)
    {
        for(const auto& [e, label]: _grid[detail::idx(u / _block_size, v_id, _n_grid)])
        {
            if(e.first == u)
            {
                co_yield { e.second, label };
            }
        }
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += std::min(_block_size, _n);
#endif
}

template <typename Label>
auto edge_grid<Label>::in_neighbors(uint32_t v) const -> vertex_stream
{
    for(uint32_t u_id = 0; u_id < _n_grid; ++u_id)
    {
        for(const auto& [e, label]: _grid[detail::idx(u_id, v / _block_size, _n_grid)])
        {
            if(e.second == v)
            {
                co_yield { e.first, label };
            }
        }
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += std::min(_block_size, _n);
#endif
}

template <typename Label>
auto edge_grid<Label>::edges() const -> edge_stream
{
    for(uint32_t u_id = 0; u_id < _n_grid; ++u_id)
    {
        for(uint32_t v_id = 0; v_id < _n_grid; ++v_id)
        {
            for(const auto& [e, label]: _grid[detail::idx(u_id, v_id, _n_grid)])
            {
                co_yield { e.first, e.second, label };
            }
        }
    }
#ifdef BENCHMARK_ON
    uint32_t b_size = std::min(_block_size, _n);
    total_sequential_searches += b_size * b_size;
#endif
}

template <typename Label>
auto edge_grid<Label>::out_degree(uint32_t u) const -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& v : out_neighbors(u))
    {
        ++degree;
    }

    return degree;
}

template <typename Label>
auto edge_grid<Label>::in_degree(uint32_t v) const -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& u : in_neighbors(v))
    {
        ++degree;
    }

    return degree;
}

template <typename Label>
auto edge_grid<Label>::n_vertices() const -> uint32_t
{
    return _n;
}

template <typename Label>
auto edge_grid<Label>::n_edges() const -> uint32_t
{
    return _m;
}

} // namespace graph_structures
