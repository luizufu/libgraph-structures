
namespace graph_structures
{
template <typename Label>
adj_list<Label>::adj_list(uint32_t n)
    : _lists(n)
    , _n_edges(0)
{
#ifdef BENCHMARK_ON
    total_stack_usage += sizeof(_n_edges);
#endif
}

template <typename Label>
adj_list<Label>::~adj_list()
{
#ifdef BENCHMARK_ON
    total_stack_usage -= sizeof(_n_edges);

    for(const auto& map : _lists)
    {
        total_heap_usage -= map.size() * (2*sizeof(uint32_t) + sizeof(Label));
    }
#endif
}

template <typename Label>
void adj_list<Label>::insert_edge(uint32_t u, uint32_t v, const Label& label)
{
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    if(_lists[u].insert({v, label}).second)
    {
        ++_n_edges;
#ifdef BENCHMARK_ON
        total_heap_usage += 2*sizeof(uint32_t) + sizeof(Label);
#endif
    }
}

template <typename Label>
void adj_list<Label>::delete_edge(uint32_t u, uint32_t v)
{
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    if(_lists[u].erase(v) > 0)
    {
        --_n_edges;
#ifdef BENCHMARK_ON
        total_heap_usage -= 2*sizeof(uint32_t) + sizeof(Label);
#endif
    }
}

template <typename Label>
void adj_list<Label>::delete_out_neighbors(uint32_t u)
{
    _n_edges -= _lists[u].size();
    auto& map = _lists[u];
#ifdef BENCHMARK_ON
        total_heap_usage -= map.size() * (2*sizeof(uint32_t) + sizeof(Label));
#endif
    map.clear();
}

template <typename Label>
void adj_list<Label>::delete_in_neighbors(uint32_t v)
{
    for(uint32_t u = 0; u < _lists.size(); ++u)
    {
        delete_edge(u, v);
    }
}

template <typename Label>
auto adj_list<Label>::has_edge(uint32_t u, uint32_t v) const -> bool
{
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    return _lists[u].contains(v);
}

template <typename Label>
auto adj_list<Label>::label(uint32_t u, uint32_t v) -> Label*
{
    auto& list = _lists[u];
    auto it = list.find(v);
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    return it != list.end() ? &it->second : nullptr;
}

template <typename Label>
auto adj_list<Label>::label(uint32_t u, uint32_t v) const -> const Label*
{
    const auto& list = _lists[u];
    auto it = list.find(v);
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    return it != list.end() ? &it->second : nullptr;
}

template <typename Label>
auto adj_list<Label>::out_neighbors(uint32_t u) const -> vertex_stream
{
    for(auto& [v, label]: _lists.at(u))
    {
        co_yield { v, label };
    }
}

template <typename Label>
auto adj_list<Label>::in_neighbors(uint32_t v) const -> vertex_stream
{
    for(uint32_t u = 0; u < _lists.size(); ++u)
    {
        auto it = _lists[u].find(v);
#ifdef BENCHMARK_ON
        ++total_binary_searches;
#endif
        if(it != _lists[u].end())
        {
            co_yield { u, it->second };
        }
    }
}

template <typename Label>
auto adj_list<Label>::edges() const -> edge_stream
{
    for(uint32_t u = 0; u < _lists.size(); ++u)
    {
        for(const auto& [v, label]: _lists[u])
        {
            co_yield { u, v, label };
        }
    }
}

template <typename Label>
auto adj_list<Label>::out_degree(uint32_t u) const -> uint32_t
{
    return _lists[u].size();
}

template <typename Label>
auto adj_list<Label>::in_degree(uint32_t v) const -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& adj: _lists)
    {
#ifdef BENCHMARK_ON
        ++total_binary_searches;
#endif
        if(adj.contains(v))
        {
            ++degree;
        }
    }

    return degree;
}

template <typename Label>
auto adj_list<Label>::n_vertices() const -> uint32_t
{
    return _lists.size();
}

template <typename Label>
auto adj_list<Label>::n_edges() const -> uint32_t
{
    return _n_edges;
}

} // namespace graph_structures
